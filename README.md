<!-- @format -->

# Introduction

AEM 專案

### Technology Stack

Yarn, NEXT, React, Typescript, Tailwind

# Getting Started

### Setup your project

```
1. Upgrade your nodejs to 16.19.1
2. Run `npm install yarn --g`
3. Run `yarn`
4. Run `npx husky install`
```

`Tip1`: [Yarn](https://yarnpkg.com/) docs.

### pre-commit not working on macOS

https://dev.to/duflow89/git-pre-commit-hook-not-working-macos-4dbc

```
chmod ug+x .husky/*
chmod ug+x .git/hooks/*
```

### Available commands

##### Run in development mode

```
yarn dev
```

##### Create production build

```
yarn build
```

##### Serve production build locally

```
yarn start
```

##### Run ESLint linting

```
yarn lint
```

##### Run ESLint linting and fix

```
yarn lint:fix
```

`Tips`: Please follow [Airbnb JS Style Guide](https://github.com/airbnb/javascript) and [Airbnb React Style Guide](https://github.com/airbnb/javascript/tree/master/react)

##### Run ESLint linting and type check

```
yarn validate
```

`Tips`: This command will be automatically executed before each git push.

### VSCode Extensions

Install all extensions on your recommend view.

![recommend view](https://pialm01/tfs/DefaultCollection/b2e5ff3c-baed-4244-9ffa-01332bc24c9f/_apis/git/repositories/c1ae789d-ce03-4ec7-902e-c2371c62916e/Items?path=%2F.attachments%2Fimage-8b60474b-52e6-461f-befa-9fdc735b06af.png&versionDescriptor%5BversionOptions%5D=0&versionDescriptor%5BversionType%5D=0&versionDescriptor%5Bversion%5D=wikiMaster&download=false&resolveLfs=true&%24format=octetStream&api-version=5.0-preview.1)

### Git Branch Naming Rules

{feat/fix/improve/support}/{story-id}-{description}

feat : 功能開發
fix : Bug 修復
improve : Refactor / Docs / Test
support : 派遣專用

##### Tips

以上 pattern 僅公司內部 Repo 使用，派遣的 Repo 依 {story-id}-{description} 即可。

### Git Commit Rules

Please follow [config-conventional](https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-conventional)

```
<type>[optional scope]: <description> #<task id>

[optional body]

[optional footer(s)]
```

##### Example

```
build(vite): add splitVendorChunkPlugin for vite #649283

body with multiple lines
but still no line is too long

BREAKING CHANGE: footer with multiple lines
but still no line is too long
```

##### type-enum

```
[
  'build',     // 建置工具 or 補助工具 (ex: webpack, vite, lint, IIS)
  'ci',        // CI 腳本 or 設定
  'docs',      // 文件類 / storybook
  'feat',      // 新功能
  'fix',       // bugs 修復
  'perf',      // 效能相關
  'refactor',  // 重構且不影響功能
  'revert',    // revert commit
  'style',     // 樣式 / theme
  'test'       // unit test / e2e
];
```

# File Rules

1. 使用 Nextjs 目的主要是為了達到之後可以讓頁面擁有 SEO 的能力因此在專案開發上須嚴格注意:
    - 無須與使用者互動的元件及頁面請勿隨意使用 ‘use client’ 在檔案開頭
    - ‘use client’ 請盡量使用在末端元件上
2. 凡是未來有可能會需要重複使用的元件，請都拆到 /components 資料夾，如: button, carousel、card、badge … etc 可參考元件網站(https://ui.shadcn.com/docs/components/accordion)
3. Typescript 禁止使用 any 型別及請勿隨意使用 unknown
4. 如專案有需要使用到第三方套件須注意以下幾點:
    - 需與國泰前端人員告知套件使用目的
    - 需確認是否有資安疑慮，必須通過 snyk 驗證，如有出現風險，請避開該套件或該套件特定版本。
        - Snyk 網址: https://security.snyk.io/package/npm/{套件名稱}
        - EX: https://security.snyk.io/package/npm/react-hook-form
    - Github Repo Check
        - License 是否為 MIT
        - Star確認星星數,
        - Issues 問題是否都會有人協助回答或修正,
        - Last updated time 是否還有持續維護,
        - Weekly Downloads下載量多不多
        - 文件完整性
        - 有沒有寫測試，沒寫不要用

# File Structure

```
root/
├───README.md
├───.gitignore
├───.prettierignore
├───.prettierrc.json
├───package.json
├───.eslintrc                eslint 設定檔
├───.eslintignore            eslint 忽略檔
├───commitlint.config.js     commit msg lint 設定檔
├───.env[.production]        環境變數設定
├───tsconfig.json            typescript 設定檔
├───next.config.js           next 設定檔，主要專案建置工具
│
├───.husky/
│   ├───commit-msg           git hook 設定檔 (commit之前)
│   ├───pre-commit           git hook 設定檔 (commit之前)
│   └───_/
│       └───husky.sh         git hook 執行檔
│
└───.vscode/
│   ├───extensions.json      vscode 擴充元件推薦列表
│   ├───settings.json        vscode 設定檔(formatter, formatOnSave, cSpell whitelist)
│   │   ...
│
├───public/                  靜態檔案
│   ├───images/
│   ├───fonts/               字型
│   ├───venders/             第三方套件
│   │   ...
│
└───src/
    ├───app/                    僅能放路由相關的檔案
    │   ├─ layout.tsx           Root Layout
    │   ├─ page.tsx             Root Page
    │   ├─ loading.tsx          Root Page
    │   ├─ error.tsx            Root Layout 子層出錯時，會顯示 error
    │   ├─ global-error.tsx     當 Root Layout | Root Template 出錯時，會顯示 global error
    │   ├─ not-found.tsx        URL 找不到時的全站顯示畫面
    │   ├───(groupName) | [paramName] | {路由}/
    │   │   │   ├───page.tsx
    │   │   │   ├───layout.tsx
    │   │   │   ├───template.tsx
    │   │   │   │   ...
    │   │   │   components/                             特定路由的元件
    │   │   │   │   ├───{Component}.tsx
    │   │   │   │   ├───type.ts
    │   │   │   │   ├───index.ts
    │   │   │   │   ...
    │   │   │   (groupName) | [paramName] | {路由}/ 　　子路由
    │   │   │   │   ├───page.tsx
    │   │   │   │   ├───layout.tsx
    │   │   │   │   ├───template.tsx
    │   │   │   │   ...
    │   │
    ├───api/                                  Handler route
    │   │   {功能名稱}/
    │   │   ├───route.ts
    │   │   │   ...
    │
    ├───actions/                              Server action || fetch api data
    │   │   {功能名稱}/
    │   │   ├───actions.ts
    │   │   ├───fetch.ts
    │   │   ├───type.ts
    │   │   │   ...
    │
    ├───components/                             全站共用元件
    │   │   {component}/
    │   │   ├───{Component}.tsx                 UI components
    │   │   ├───type.ts
    │   │   ├───index.ts
    │   │   ├───{subComponent}/
    │   │   ...
    │
    ├───hooks/
    │   │   {hooks}/
    │   │   ├───{hooks}.tsx　
    │   │   ├───index.ts
    │   │   ├───type.ts
    │   │   ...
    │
    ├───contexts/          狀態管理
    │   │   {feature}/
    │   │   ├───{feature}Context.tsx
    │   │   ├───{feature}Provider.tsx
    │   │   ├───type.ts
    │   │   ...
    │
    ├─── styles/            共用樣式檔案
    │   │   globals.scss    主樣式檔案
    │   │   ...
    │
    ├───types/              共用類型別檔
    │   ├───{features}.types.ts
    │   ├───{features}.enums.ts
    │   │   ...
    │
    ├───utils/             如：helpers/common/constants
    │   │   ...
    │
    ├───vite-env.ts         vite 環境變數型別定義
    │   ...

```

### 命名規則

資料夾與`非`元件類檔案統一小駝峰，元件類(page/component)檔案統一大駝峰

#### 共用元件 /src/components/...

-   格式：/src/components/{元件名稱}/{元件名稱}.tsx
-   Rule：`經常重複使用`的共用元件或`全站顯示`的元件放在 components 資料夾底下，並依據功能名稱命名資料夾，相同類型或相關元件會放置在同一個功能資料夾中，
    如: button, carousel、card、badge … etc 可參考元件網站(https://ui.shadcn.com/docs/components/accordion)
-   範例：
    -   /src/components/button/Button.tsx
    -   /src/components/button/ScrollTopButton.tsx
    -   /src/components/button/type.ts
    -   /src/components/button/index.ts
    -   ...

#### 共用 Hook /src/hooks/...

-   格式：/src/hooks/{hook名稱}/{hook名稱}.tsx
-   Rule：`經常重複使用`的 Hook 放在 hooks 資料夾底下，並依據功能名稱命名資料夾
-   範例：
    -   /src/hooks/useFileUpload/useFileUpload.tsx
    -   /src/hooks/useFileUpload/type.ts
    -   /src/hooks/useFileUpload/index.ts
    -   ...

#### 共用 function /src/utils/...

-   格式：/src/hooks/{hook名稱}.tsx
-   Rule：`經常重複使用`的 function 放在 utils 資料夾底下
-   範例：

    -   較多同一類型的 function 則在 utils 中建立功能資料夾並寫清楚使用說明，Ex: maskName、maskBirth、maskPhone ...etc

        -   /src/utils/maskHide/maskHide.ts
        -   /src/utils/maskHide/index.ts
        -   /src/utils/maskHide/README.md

    -   較小且不同類型的 function，則可統一放置在 tool.ts 中，Ex: numberAddThousandsComma、numberMathRound、isNegative、validateMaxLength ... etc
        -   /src/utils/tool.ts
