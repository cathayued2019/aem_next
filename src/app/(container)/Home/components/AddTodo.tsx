/** @format */

import React from 'react';

import { addTodo } from 'src/actions/todo/action';
import Input from 'src/components/input';
import PlusButton from './PlusButton';

const AddTodo = () => (
    <form action={addTodo} className="mb-5">
        <div className="my-4 flex  justify-end space-x-2 pt-3 align-middle">
            <Input className="pl-4" name="title" />
            <PlusButton />
        </div>
    </form>
);
export default AddTodo;
