/** @format */

'use client';

import React, { useState } from 'react';
import { TodoType } from 'src/actions/todo/type';
import { updateTodo } from 'src/actions/todo/action';
import { useRouter } from 'next/navigation';
import Input from 'src/components/input';

type TodoInputType = {
    data: TodoType;
};

const TodoInput: React.FC<TodoInputType> = ({ data }) => {
    const [val, setVal] = useState(data.title);
    const [isEdit, setIsEdit] = useState(false);
    const router = useRouter();

    const handleBlur = async (e: React.FormEvent<HTMLInputElement>) => {
        await updateTodo({ ...data, title: e.currentTarget.value });
        router.refresh();
    };

    return (
        <>
            <Input
                className={`flex-1 outline-none  ${isEdit ? 'block' : 'hidden'}`}
                onBlur={(e) => handleBlur(e)}
                value={val}
                onChange={(e) => setVal(e.currentTarget.value)}
            />
            <button className={isEdit ? 'hidden' : 'block'} onClick={() => setIsEdit(true)}>
                {val}
            </button>
        </>
    );
};

export default TodoInput;
