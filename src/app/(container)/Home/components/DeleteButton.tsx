/** @format */

'use client';

import React from 'react';
import Image from 'next/image';

import { deleteTodo } from 'src/actions/todo/action';
import Button from 'src/components/button';
import { useRouter } from 'next/navigation';

const DeleteButton = ({ id }: { id: string }) => {
    const router = useRouter();

    return (
        <Button
            onClick={async () => {
                await deleteTodo(id);
                router.refresh();
            }}
        >
            <Image src="/images/icons/icon-delete.svg" width="20" height="20" alt="delete item" />
        </Button>
    );
};

export default DeleteButton;
