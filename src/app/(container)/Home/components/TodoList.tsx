/** @format */

import React from 'react';

import { TodoListType } from 'src/actions/todo/type';
import TodoItem from './TodoItem';

export type TodoListProps = {
    title: string;
    datas: TodoListType;
};

const TodoList: React.FC<TodoListProps> = ({ title, datas }) => (
    <section>
        <div className="mb-3 flex justify-between">
            <h2 className="text-xl">{title}</h2>
        </div>
        <div className="space-y-3">
            {datas.map((data) => (
                <TodoItem key={data.id} data={data} />
            ))}
        </div>
    </section>
);
export default TodoList;
