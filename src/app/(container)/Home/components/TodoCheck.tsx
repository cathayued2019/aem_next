/** @format */

'use client';

import React from 'react';
import { useRouter } from 'next/navigation';

import { updateTodo } from 'src/actions/todo/action';
import { TodoType } from 'src/actions/todo/type';

const TodoCheck = ({ data }: { data: TodoType }) => {
    const router = useRouter();

    const handleChange = async (e: React.FormEvent<HTMLInputElement>) => {
        await updateTodo({ ...data, isDone: e.currentTarget.checked });
        router.refresh();
    };
    return <input type="checkbox" className="mr-5" onChange={handleChange} checked={data.isDone} />;
};

export default TodoCheck;
