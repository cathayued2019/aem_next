/** @format */

import React from 'react';

import { TodoType } from 'src/actions/todo/type';
import DeleteButton from './DeleteButton';
import TodoInput from './TodoInput';
import TodoCheck from './TodoCheck';

type TodoItemProps = {
    data: TodoType;
};

const TodoItem: React.FC<TodoItemProps> = ({ data }) => (
    <div className="flex items-center justify-between rounded-md bg-white p-2 px-4">
        <span className="mr-2">{data.id}</span>
        <div className="flex w-full">
            <TodoCheck data={data} />
            <TodoInput data={data} />
        </div>
        <div className="flex items-center">
            <DeleteButton id={data.id} />
        </div>
    </div>
);
export default TodoItem;
