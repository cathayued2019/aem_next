/** @format */

'use client';

import React from 'react';
import Image from 'next/image';
import Button from 'src/components/button';

const PlusButton = () => (
    <Button type="submit" className="rounded-md bg-gray-300 p-2">
        <Image src="/images/icons/icon-plus.svg" width="20" height="20" alt="add one todo item" />
    </Button>
);

export default PlusButton;
