/** @format */

import React, { useMemo } from 'react';
import { TodoListType } from 'src/actions/todo/type';
import TodoList from './TodoList';
import AddTodo from './AddTodo';

type ContentType = {
    datas: TodoListType;
};

const Content: React.FC<ContentType> = ({ datas }) => {
    const eventData = useMemo(() => datas.filter((item) => !item.isDone), [datas]);
    const completeData = useMemo(() => datas.filter((item) => item.isDone), [datas]);

    return (
        <>
            <AddTodo />
            <div className="space-y-5">
                <TodoList title="EVENT" datas={eventData} />
                <TodoList title="COMPLETE" datas={completeData} />
            </div>
        </>
    );
};

export default Content;
