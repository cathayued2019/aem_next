/** @format */
import React from 'react';
import getTodoList from 'src/actions/todo/fetch';
import Content from './components/Content';

const HomePage = async () => {
    const data = await getTodoList();

    return (
        <section>
            <Content datas={data} />
        </section>
    );
};
export default HomePage;
