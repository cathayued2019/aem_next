/** @format */

import React, { InputHTMLAttributes, forwardRef } from 'react';

type InputProps = InputHTMLAttributes<HTMLInputElement>;
export type Ref = HTMLInputElement;

const Input = forwardRef<Ref, InputProps>((props, ref) => {
    const { className, ...rest } = props;

    return <input {...rest} ref={ref} className={`${className} w-full rounded-md outline-none`} />;
});

export default Input;
