import React from 'react';

const Header: React.FC = () => (
    <header className="border-b-2 pb-5 text-center text-2xl">My Todo List</header>
);

export default Header;
