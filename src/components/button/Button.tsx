/** @format */
import React, { ButtonHTMLAttributes, PropsWithChildren, forwardRef } from 'react';

export type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & PropsWithChildren;
export type Ref = HTMLButtonElement;

const Button = forwardRef<Ref, ButtonProps>((props, ref) => {
    const { children, ...rest } = props;
    return (
        <button {...rest} ref={ref}>
            {children}
        </button>
    );
});

export default Button;
