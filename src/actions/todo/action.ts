/** @format */

'use server';

import { revalidatePath } from 'next/cache';
import { TodoType } from './type';

const domain = 'http://localhost:3005/todos/';

export async function addTodo(data: FormData) {
    const title = data.get('title');

    const res = await fetch(`${domain}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ title, isDone: false }),
    });

    await res.json();
    revalidatePath('/Home');
}

export async function updateTodo(data: TodoType) {
    const res = await fetch(`${domain}${data.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });

    await res.json();
}

export async function deleteTodo(id: string) {
    const res = await fetch(`${domain}${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
        },
    });

    await res.json();
}
