/** @format */

export default async function getTodoList() {
    const result = await fetch('http://localhost:3005/todos', {
        cache: 'no-cache',
    });

    if (!result.ok) {
        throw new Error('api fail');
    }

    return result.json();
}
