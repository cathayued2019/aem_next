export type TodoType = {
    title: string;
    isDone: boolean;
    desc: string;
    date: string;
    id: string;
};

export type TodoListType = TodoType[];
